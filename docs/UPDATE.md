## Test production server DB upgrade
Save current staging changes into another branch and reset staging to master
```bash
git fetch --all
git checkout staging
git branch save-staging
git reset --hard origin/master
```

Delete the harbor-staging helm deployment
```bash
helm list -A
helm delete --namespace flux flux-staging
kubectl --all-namespaces=true delete hr --all
# Wait that harbor-staging deployment gets deleted by helm-operator
```

Drop all the harbor-staging databases
```bash
export DB_HOSTNAME="dbod-harbordb-t.cern.ch"
export DB_PORT="6601"
export DB_USERNAME="admin"
# dropdb -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} registry
# dropdb -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} notary_signer
# dropdb -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} notary_server
```

Create a copy of the production database
```bash
mkdir harbor-dbs && cd harbor-dbs
export DB_HOSTNAME="dbod-harbordb.cern.ch"
export DB_PORT="6602"
export DB_USERNAME="admin"
pg_dump -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} registry > registry.sql
pg_dump -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} notary_signer > notary_signer.sql
pg_dump -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} notary_server > notary_server.sql
```

Create and load the production data into harbor-staging databases
```bash
export DB_HOSTNAME="dbod-harbordb-t.cern.ch"
export DB_PORT="6601"
export DB_USERNAME="admin"
psql -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} -c "CREATE DATABASE registry;"
psql -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} -c "CREATE DATABASE notary_signer;"
psql -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} -c "CREATE DATABASE notary_server;"
psql -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} registry < registry.sql
psql -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} notary_signer < notary_signer.sql
psql -h ${DB_HOSTNAME} -p ${DB_PORT} -U ${DB_USERNAME} notary_server < notary_server.sql
```

At this point harbor-staging data is the same as prod harbor and we should validate that everything seems ok. We just need to bring up the services the same way they are in master. for this we do:
```bash
git push origin staging -f
helm install flux-staging fluxcd/flux --namespace flux  --values flux-values-staging.yaml
```

The last step is to upgrade staging and check that everything runs smothly
```bash
git checkout staging
git reset --hard save-staging
git push origin staging
```

NOTE: You need to change the Harbor admin password from the production to the staging.
For this, access registry-staging.cern.ch -> 'Login Via Local DB' - > admin/prod
In the user options change the current password to the staging one


## PRODUCTION upgrade
- Create a merge request with the staging changes into master
- Ask a peer to review the MR. Then merge your changes
