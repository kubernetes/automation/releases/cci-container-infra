apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
    name: harbor-staging
    namespace: staging
    annotations:
        fluxcd.io/automated: "true"
spec:
    releaseName: harbor-staging
    chart:
        git: https://gitlab.cern.ch/kubernetes/automation/releases/cci-container-infra
        path: charts/cci-container-infra
        ref: staging
    values:
        cern:
            loadbalancer:
                registry:
                    httpsPoolID: ENC[AES256_GCM,data:JLM2/60nykWjkojfsmLJjwqXkxCEqx6oTFNIW7j4MxUTocjY,iv:j+bf3PXXqkJHJ1rbqRnP3CyZfVearTngpDZPLdtIzS8=,tag:+Jgk9nNKwNShQocOnjiLVQ==,type:str]
                    httpPoolID: ENC[AES256_GCM,data:uajRfbgg5iEreaoMVlI4rUr6+2BmOYiH01abqSsU7EJ9T+Rv,iv:wxXZmuTP1JKTZInfmA3mji209bdOkeFa5vXdQY+uTRc=,tag:B8Rc43OLg2lHCn6AEwObnQ==,type:str]
            oidc:
                clientID: ENC[AES256_GCM,data:GmAttWsSXaMFhIdenYx79A==,iv:e88Es9Svz9J+5H5lz3fhFDQGdPr4XZBbQcskPVKUxo4=,tag:ogbzpOs/nQ8CfrlFK8ghOg==,type:str]
                clientSecret: ENC[AES256_GCM,data:OY/+lf14oY8f0x+taA/el+vGd2tew3jI3XunF2GIbENS1mHn,iv:fBxGExOGdhP7FGpsILNweHiDhCMZCtWcklB2y+vvvDw=,tag:fF0UHk3ce7KM9Ta5FpoxhQ==,type:str]
            tls:
                acme:
                    server: https://acme-staging-v02.api.letsencrypt.org/directory
        harbor:
            expose:
                type: ingress
                tls:
                    enabled: true
                    certSource: secret
                    secret:
                        secretName: ENC[AES256_GCM,data:gB3Iw7+BUv0EXR4d/1Y9oRu0QOwaQo28omrhhNo=,iv:IjltRVSXk1OPRwVUcCi4HWx1MGilaVRutsPQ/SjZgik=,tag:CU0wVjviOrwkzVVQMkYLdw==,type:str]
                ingress:
                    hosts:
                        core: registry-staging.cern.ch
                        notary: notary-staging.cern.ch
                    annotations:
                        ingress.kubernetes.io/ssl-redirect: "true"
                        ingress.kubernetes.io/proxy-body-size: "0"
                        nginx.ingress.kubernetes.io/ssl-redirect: "true"
                        nginx.ingress.kubernetes.io/proxy-body-size: "0"
                        cert-manager.io/issuer: letsencrypt
                        nginx.ingress.kubernetes.io/proxy-request-buffering: "off"
            externalURL: https://registry-staging.cern.ch
            persistence:
                persistentVolumeClaim:
                    jobservice:
                        existingClaim: staging-cci-container-infra-harbor-jobservice
                    redis:
                        existingClaim: staging-cci-container-infra-harbor-redis
                    trivy:
                        storageClass: geneva-cephfs-testing
                imageChartStorage:
                    type: s3
                    s3:
                        bucket: registry-staging
                        accesskey: ENC[AES256_GCM,data:tBCxihR8eUjHthqyPgfsLo+5wl2Z9TwrWd2LWs3yw9M=,iv:ExwjmOLuBYm33VWW5oghYi2/Ge6B7pxBQxwwvWElds0=,tag:Mnul45shtKsCS4Na6pyLYQ==,type:str]
                        secretkey: ENC[AES256_GCM,data:tFiDjNVbrVxMPjXrWH43/gqQEpoq5pbYCYdSEmGHXaM=,iv:TGclokkuXPiaB8H+hXQ22l51SjwiLSzJdLIWa+xDDns=,tag:Jv0T2cA39pY9pX+B0N07WQ==,type:str]
            logLevel: debug
            harborAdminPassword: ENC[AES256_GCM,data:IzFJtng/SNgvKogcJv3U1IKLOVE6pAJB42ebdbNQaV0=,iv:HZ4qpaq4Chbb2C3fhvdcRkMvJTt6TbEEbwhFSfslZhE=,tag:Ev6YQD6dC007x5zYAAKPag==,type:str]
            secretKey: ENC[AES256_GCM,data:SfLqikZn0D1qd4arIG4x4w==,iv:28iEgjtvKAdrRR16GLV19gxzjuUUdNy/hk4qPP6hMZI=,tag:/4GbstgtCFISmLkKqrfUJA==,type:str]
            portal:
                replicas: 3
            core:
                replicas: 3
            jobservice:
                replicas: 3
            registry:
                replicas: 3
                credentials:
                    username: admin
                    password: ENC[AES256_GCM,data:Q6K5rI/2HrvX5sJA9+9yMqBvftg9v9fFgXRtautRU5o=,iv:bjoJJZFD39Jkaqkg2aOMOLDTc6LlecpUkdhEN+6+2Io=,tag:FkWqhquCRcrUqkLYxfOSvQ==,type:str]
                    htpasswd: ENC[AES256_GCM,data:8FHqlSXhKnD7KDtWLII5xIi2WD1KAffiCCStckqlcbZgzcYc60UvggAhkr+s9RTBFzt22z5BjbpAWN+t/bwqQQ3a,iv:DKoTd+DE7BmrY40hAEB52eBa2YiI4y3UrHXxySUaEEw=,tag:DdyDzctWFr89rI7vU7IOlA==,type:str]
            chartmuseum:
                replicas: 3
            database:
                type: external
                external:
                    host: dbod-harbordb-t.cern.ch
                    port: "6601"
                    username: admin
                    password: ENC[AES256_GCM,data:jC4pPQGfUYnUPlNeIGWfEmqyYkiY6WbXy1gHRzz8rds=,iv:YXrV4DGLIkKjhad+/km2XdHdsqGmENgQLLdoAjBCh6E=,tag:bOqM3OY4DGxHlv5cwAocvg==,type:str]
                    sslmode: require
                maxIdleConns: 5
                maxOpenConns: 20
            metrics:
                enabled: true
sops:
    kms: []
    gcp_kms: []
    azure_kv: []
    hc_vault: []
    barbican:
    -   secret_href: https://openstack.cern.ch:9311/v1/secrets/9faa1229-bddf-4b3e-a05b-ef43ccf72527
        created_at: '2022-07-27T09:29:16Z'
        enc: ZA+vfXXwQceLFEAnppfv9TLdR9J/QbFp1DaqPhCZwMD9x8H/Ljr5GjZp5F6Cp6iZ
    lastmodified: '2023-04-03T09:04:17Z'
    mac: ENC[AES256_GCM,data:xl/APqoY00dahbyOpm4WZuirQ/TSYIrIDOuRW4Sno5vR07t48WLmd+H3ja9iv0mWtnCr+XpYX7b9VNr79ke8omhaJ4Lhjr6Nn7jcFSvnON4EvNkQpIMh4r8ncsHwv0s00krBZx5081icw8nQNB9Pe+d/VBYMgCpg1VZunW4eRy8=,iv:/H5b8JKj3kkKx7Wk3rzPmf3CQ9o5DVNE/GExS3K3fMU=,tag:XFM0KvXOA61nc1bmXGTbXg==,type:str]
    pgp: []
    encrypted_regex: ^(transport_url)|(.*[Pp]assword)|(encryptMe.*)|(.*[Pp]asswd)|(.*[Kk]ey)|(clientSecret)|(secret)|(.*ID)$
    version: 3.5.0-1.cern
