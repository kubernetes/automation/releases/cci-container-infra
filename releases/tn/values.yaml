apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
    name: cci-container-infra
    namespace: prod
    annotations:
        fluxcd.io/automated: "true"
spec:
    releaseName: harbor
    chart:
        git: https://gitlab.cern.ch/kubernetes/automation/releases/cci-container-infra
        path: charts/cci-container-infra
        ref: master
    values:
        cern:
            loadbalancer:
                namespace: kube-system
                registry:
                    httpsPoolID: null
                    httpPoolID: null
            oidc:
                enabled: true
                clientID: ENC[AES256_GCM,data:TVSc6GD0e3qYvFo=,iv:BYgA8s8QxhaMCmajGWh6NGPAPhPXebqPgffKxZ6ctUM=,tag:w8G8aVr8y9NpeqocXyCVRQ==,type:str]
                clientSecret: ENC[AES256_GCM,data:GuY9apI2fr/wghydghTelOoma1hPfE4H6pribnDZIh5bMa/j,iv:+IeqNUhiGu7KD0lXVyviRol14iGO8aLkBGzOw1fPT8A=,tag:EcGnGfT4kQl5tfzKR6DJIA==,type:str]
            tls:
                acme:
                    email: ricardo.rocha@cern.ch
                    # Set the way how to expose the service. Set the type as "ingress",
                    # "clusterIP", "nodePort" or "loadBalancer" and fill the information
                    # in the corresponding section
                    ## Grafana Monitoring Dashboard
                    ##
            dashboards:
                enabled: true
                namespace: kube-system
                labels:
                    grafana_dashboard: "1"
        harbor:
            expose:
                type: ingress
                tls:
                    enabled: true
                ingress:
                    hosts:
                        core: registry-tn.cern.ch
                        notary: notary-tn.cern.ch
                    annotations:
                        ingress.kubernetes.io/ssl-redirect: "true"
                        traefik.ingress.kubernetes.io/frontend-entry-points: https,http
                        ingress.kubernetes.io/proxy-body-size: "0"
                        nginx.ingress.kubernetes.io/ssl-redirect: "true"
                        nginx.ingress.kubernetes.io/proxy-body-size: "0"
                        # The name of ClusterIP service
            externalURL: https://registry-tn.cern.ch
            persistence:
                persistentVolumeClaim:
                    registry:
                        existingClaim: ""
                        storageClass: openebs-hostpath
                        subPath: ""
                        accessMode: ReadWriteOnce
                        size: 5Gi
                    chartmuseum:
                        existingClaim: ""
                        storageClass: openebs-hostpath
                        subPath: ""
                        accessMode: ReadWriteOnce
                        size: 5Gi
                    jobservice:
                        existingClaim: ""
                        storageClass: openebs-hostpath
                        subPath: ""
                        accessMode: ReadWriteOnce
                        size: 1Gi
                    database:
                        existingClaim: ""
                        storageClass: openebs-hostpath
                        subPath: ""
                        accessMode: ReadWriteOnce
                        size: 1Gi
                    redis:
                        existingClaim: ""
                        storageClass: openebs-hostpath
                        subPath: ""
                        accessMode: ReadWriteOnce
                        size: 1Gi
                    trivy:
                        existingClaim: ""
                        storageClass: openebs-hostpath
                        subPath: ""
                        accessMode: ReadWriteOnce
                        size: 5Gi
                imageChartStorage:
                    disableredirect: true
                    type: s3
                    s3:
                        regionendpoint: http://minio-tn.cern.ch
                        bucket: harbor
                        accesskey: ENC[AES256_GCM,data:S2EgjCxOwnEqJaeEdcCNZdCHLKI=,iv:6ji7kGuHQ78f6TM4Q9Kh1yOQr2dRKGEMF3DCKXlgs0c=,tag:JM1euDbpaZ5JBNAQVjCnQA==,type:str]
                        secretkey: ENC[AES256_GCM,data:uPUlGsJSD5oF90W3c1/c0h8RivMOr40TY/RiZR0ZpeUgEWaeo1TX2w==,iv:hnkLnp0C5QKKqmC20BsUfgl1PpSmERtpQ5//I/RTCUM=,tag:4DvS3hjOIUiQhgkKv6R+hg==,type:str]
                        #encrypt: false
                        secure: false
                        skipverify: true
            logLevel: debug
            harborAdminPassword: ENC[AES256_GCM,data:xjYKuVV0,iv:YCsKmQL5y8LI7ZSv8+lcd48EtqJM0YVA0cPtMCaLl20=,tag:H/oMj/yBtBP2zryck5VAvA==,type:str]
            portal:
                image:
                    repository: tn-regitry.cern.ch/harbor-portal
                    tag: v2.2.1
                replicas: 1
            core:
                image:
                    repository: tn-regitry.cern.ch/harbor-core
                    tag: v2.2.1
                replicas: 1
                ## Startup probe values
            jobservice:
                image:
                    repository: tn-regitry.cern.ch/harbor-jobservice
                    tag: v2.2.1
                replicas: 1
            registry:
                credentials:
                    username: admin
                    password: ENC[AES256_GCM,data:2TfNrJYf,iv:CnFgmTqSOZuSIaDACbL8RBkHjNIJHbGLYlvVc/+yDvs=,tag:QBrEj2XQizntT2mB1GXk9w==,type:str]
                    htpasswd: ENC[AES256_GCM,data:jPFxXXSV06pbakFH7xrbn3jdQnuGjYogtDukMKNUUUBcnukK5OHjkJ5d+oYkd7OZ5BuDaURw6zxUjATV/bY0B09G,iv:SnnZgsKm4EiNzU2HLlJhm9betr/Q+ihNMIrlODPv75w=,tag:iColUWvQX9IP8wYBGOkulg==,type:str]
                registry:
                    image:
                        repository: tn-regitry.cern.ch/registry-photon
                        tag: v2.2.1
                controller:
                    image:
                        repository: tn-regitry.cern.ch/harbor-registryctl
                        tag: v2.2.1
                replicas: 1
                relativeurls: true
            chartmuseum:
                enabled: true
                image:
                    repository: tn-regitry.cern.ch/chartmuseum-photon
                    tag: v2.2.1
                replicas: 1
            trivy:
                enabled: true
                image:
                    repository: tn-regitry.cern.ch/trivy-adapter-photon
                    tag: v2.2.1
                replicas: 1
            notary:
                enabled: false
                server:
                    image:
                        repository: tn-regitry.cern.ch/notary-server-photon
                        tag: v2.2.1
                    replicas: 1
                signer:
                    image:
                        repository: tn-regitry.cern.ch/notary-signer-photon
                        tag: v2.2.1
                    replicas: 1
            database:
                type: internal
                internal:
                    serviceAccountName: ""
                    image:
                        repository: tn-regitry.cern.ch/harbor-db
                        tag: v2.2.1
            redis:
                type: internal
                internal:
                    image:
                        repository: tn-regitry.cern.ch/redis-photon
                        tag: v2.2.1
            exporter:
                replicas: 1
                image:
                    repository: tn-regitry.cern.ch/harbor-exporter
                    tag: v2.2.1
sops:
    kms: []
    gcp_kms: []
    azure_kv: []
    hc_vault: []
    barbican:
    -   secret_href: https://openstack.cern.ch:9311/v1/secrets/9faa1229-bddf-4b3e-a05b-ef43ccf72527
        created_at: '2022-07-27T09:31:28Z'
        enc: 5U3gRs014yiJDnOl6BywpDKu3IIR5qNGWcqgmFeKK3tBcRXzG+D5xu85dBCQFLIE
    lastmodified: '2022-07-27T09:52:54Z'
    mac: ENC[AES256_GCM,data:T9SVKRIdQkhXrdud8fC7JFSwtNkchFAHQsCHhpBlm1MPaXeSI1MC2tNxd3DfKVyyToKXud1Zj/BeAvU6S6drSXHEU8WaXD5ARpWtFg9EqJMj6piBe4L5xSbDICMWyT3EuNV1BYIxm/q8wWWgMsQRqJtvmVh1dGzK/O1lcte54gk=,iv:OFlSQBHhkZy8LQVtOUBVEoM5mn0K788azDhBCSkQvLA=,tag:bfKNBvWhDyYu0sGJqQggpQ==,type:str]
    pgp: []
    encrypted_regex: ^(transport_url)|(.*[Pp]assword)|(encryptMe.*)|(.*[Pp]asswd)|(.*[Kk]ey)|(clientSecret)|(secret)|(.*ID)$
    version: 3.5.0-1.cern
